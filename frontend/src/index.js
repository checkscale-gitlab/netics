import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk';
import { currentUserReducer } from './store/reducers/currentUserReducer'
import { appReducer } from './store/reducers/appReducer'
import { postReducer } from './store/reducers/postReducer'
import { userReducer } from './store/reducers/userReducer'
import { messageReducer } from './store/reducers/messageReducer'

const rootReducer = combineReducers({
    currentUser: currentUserReducer,
    app: appReducer,
    post: postReducer,
    user: userReducer,
    message: messageReducer
})

const middlewares = [logger, thunk]

// Create store with reducers and initial state
const store = createStore(
    rootReducer,
    applyMiddleware(...middlewares),
)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
