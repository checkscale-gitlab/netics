export const setupOpen = () => ({
    type: SETUP_OPEN
})

export const setupClose = () => ({
    type: SETUP_CLOSE
})

export const SETUP_OPEN = 'SETUP_OPEN'
export const SETUP_CLOSE = 'SETUP_CLOSE'