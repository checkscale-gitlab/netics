import {
    GET_USER_PENDING,
    GET_USER_SUCCESS,
    GET_USER_FAILURE,
    FOLLOW_USER_PENDING,
    FOLLOW_USER_SUCCESS,
    FOLLOW_USER_FAILURE,
    UNFOLLOW_USER_PENDING,
    UNFOLLOW_USER_SUCCESS,
    UNFOLLOW_USER_FAILURE
} from '../actions/userActions'

const initialState = {
    profile: null,
    pending: false,
    error: null
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        // Get user
        case GET_USER_PENDING:
            return {
                ...state,
                pending: true
            }
        case GET_USER_SUCCESS:
            return {
                ...state,
                pending: false,
                profile: action.user
            }
        case GET_USER_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        
        // Follow user
        case FOLLOW_USER_PENDING:
            return {
                ...state,
                pending: true
            }
        case FOLLOW_USER_SUCCESS:
            return {
                ...state,
                pending: false,
                profile: {
                    ...state.profile,
                    followers: [...state.profile.followers, action.follower]
                }
            }
        case FOLLOW_USER_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        
        // unfollow user
        case UNFOLLOW_USER_PENDING:
            return {
                ...state,
                pending: true
            }
        case UNFOLLOW_USER_SUCCESS:
            return {
                ...state,
                pending: false,
                profile: {
                    ...state.profile,
                    followers: unfollow(state, action)
                }
            }
        case UNFOLLOW_USER_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }

        default:
            return state;
    }
}


const unfollow = (state, action) => {
    return state.profile.following.filter(x => x !== action.following)
  }