import { postDeletePending, postDeleteSuccess, postDeleteFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const deletePost = (post) => {
    return dispatch => {
        dispatch(postDeletePending())
        fetch(`/api/v1/post?id=${post._id}`,
            {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postDeleteSuccess(post))
                } else {
                    dispatch(postDeleteFailure())
                    dispatch(refreshToken(deletePost, { post }))
                }
            })
            .catch(error => {
                dispatch(postDeleteFailure(error))
            })
    }
}

export default deletePost