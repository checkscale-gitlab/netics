import { postGetPending, postGetSuccess, postGetFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'

const getPost = (args, page) => {
    return dispatch => {
        dispatch(postGetPending())
        fetch(`/api/v1/${args}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                }
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postGetSuccess(res.data, page))
                } else {
                    dispatch(postGetFailure(getPost, { args, page }))
                }
            })
            .catch(error => {
                dispatch(postGetFailure(error))
            })
    }
}

export default getPost