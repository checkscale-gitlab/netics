import { commentDeletePending, commentDeleteSuccess, commentDeleteFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const createPost = (postId, commentId) => {
    return dispatch => {
        dispatch(commentDeletePending())
        fetch(`/api/v1/comment?id=${postId}&commentId=${commentId}`,
            {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(commentDeleteSuccess(postId, commentId))
                } else {
                    dispatch(commentDeleteFailure())
                    dispatch(refreshToken(createPost, { postId, commentId }))
                }
            })
            .catch(error => {
                dispatch(commentDeleteFailure(error))
            })
    }
}

export default createPost