import { sendMessagePending, sendMessageSuccess, sendMessageFailure } from '../../actions/chatActions'
import refreshToken from '../auth/refreshToken'


const sendMessage = (chatId, message, image) => {
    return dispatch => {
        dispatch(sendMessagePending())
        fetch(`/messaging/send?id=${chatId}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify({ text: message || '', image: image || '' }),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(sendMessageSuccess(chatId, res.data))
                } else {
                    dispatch(sendMessageFailure())
                    dispatch(refreshToken(sendMessage, { chatId, message, image }))
                }
            })
            .catch(error => {
                dispatch(sendMessageFailure(error))
            })
    }
}

export default sendMessage