import { logoutUserPending, logoutUserSuccess, logoutUserFailure } from '../../actions/currentUserActions'
import history from '../../../helpers/history'

const logoutUser = () => {
    return dispatch => {
        dispatch(logoutUserPending())
        fetch(`/api/auth/logout`,
            {
                method: 'POST',
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(logoutUserSuccess())
                    localStorage.removeItem('accessToken')
                    localStorage.removeItem('refreshToken')
                    history.push('/login')
                } else {
                    dispatch(logoutUserFailure())
                    history.push('/login')
                }
            })
            .catch(error => {
                dispatch(logoutUserFailure(error))
                history.push('/login')
            })
    }
}

export default logoutUser