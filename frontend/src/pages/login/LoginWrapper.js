import React, { useEffect } from 'react'
import { Col, Row, Typography } from 'antd';
import Login from './Login'
import Logo from '../../assets/Logo.png'

const styles = {
    left: {
        xs: { span: 0 },
        md: { span: 12 },
        lg: { span: 12 },
        xl: { span: 12 },
    },
    right: {
        xs: { span: 24 },
        md: { span: 12 },
        lg: { span: 12 },
        xl: { span: 12 },
    },
    col: {
        height: '100%',
        minHeight: '100vh'
    }
}

const Wrapper = (props) => {
    return (
        <Row>
            <Col {...styles.left} style={{ ...styles.col }}>
                <div style={{ height: '100vh', textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <Typography.Title style={{ fontSize: '5rem' }}>Chirper <img src={Logo} style={{ height: '4rem', marginBottom: 10 }} />
                    </Typography.Title>
                </div>
            </Col>
            <Col {...styles.right} style={{ ...styles.col }}>
                <Login />
            </Col>
        </Row>
    )
}

export default Wrapper