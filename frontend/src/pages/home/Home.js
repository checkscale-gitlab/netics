import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import createPost from '../../store/api/post/createPost'
import getPosts from '../../store/api/post/getPosts';
import deletePost from '../../store/api/post/deletePost';
import home from '../../style/Home.module.css'
import CreatePost from '../../components/post/CreatePost';
import Feed from '../../components/post/Feed';
import Loading from '../../components/layout/Loading'

const Home = (props) => {

    let [page, setPage] = useState(0)

    useEffect(() => {
        // Fetch posts
        props.getPosts(`post?page=${page}`, page)

        // Scroll listener
        window.addEventListener('scroll', infiteScroll);
        return () => window.removeEventListener('scroll', infiteScroll);
    }, [])

    const infiteScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
            setPage(++page)
            props.getPosts(`post?page=${page}`)
        }
    }

    const handlePost = (data) => {
        props.createPost(data)
    }

    return (
        <div>
            <div className={home.header}>
                <h1 className={home.title} style={{ fontSize: 24 }}><b>Home</b></h1>
            </div>
            <CreatePost mentions={true} createPost={handlePost} style={{ paddingBottom: 10, borderBottom: '1px solid #ddd' }} />
            <Feed divider={10} />
        </div>
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: createPost,
    getPosts: getPosts,
    deletePost: deletePost
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)