import React from 'react'
import profile from '../../style/Profile.module.css'
import Skeleton from 'react-loading-skeleton';


const FeedLoading = (props) => {

    return (
        <div className={profile.container}>
            <div style={{ float: 'right', flexDirection: 'column', width: '100%' }}>
                <div className={profile.horizontal}>
                    <Skeleton height={128} width={128} />
                    <div className={profile.vertical} style={{ marginLeft: 10, width: '100%' }}>
                        <Skeleton width={75} height={33} />
                        <Skeleton width="100%" height={90} />
                    </div>
                </div>
                <div className={profile.horizontal} style={{ justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <Skeleton circle={true} width={35} height={35} />
                        &nbsp;
                        <Skeleton circle={true} width={35} height={35} />
                        &nbsp;<Skeleton width={100} height={33} />
                    </div>
                    <div>
                        &nbsp;<Skeleton width={75} height={33} />
                        &nbsp;<Skeleton width={75} height={33} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FeedLoading