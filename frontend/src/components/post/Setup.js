
import React, { useState } from 'react'
import { Modal, Button, Steps, Input, Progress, Upload } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setupClose } from '../../store/actions/appActions'
import updateUser from '../../store/api/user/updateCurrentUser'
import ProfileImage from '../profile/ProfileImage'
import post from "../../style/Post.module.css";

const { Step } = Steps

const styles = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
}

const maxWords = 140

const Post = (props) => {

    const [stage, setStage] = useState(0)
    const [image, setImage] = useState('')
    const [bio, setBio] = useState('')

    const handleBioChange = (e) => {
        setBio(e.target.value)
    }

    const handleImage = (file) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = () => {
            setImage(reader.result)
        }
        return false
    }

    const handleSubmit = () => {
        props.updateUser({
            username: props.user.user.username,
            image: image,
            bio: bio
        })
        setupClose();
    }

    const stages = [
        <div style={styles}>
            <Input.TextArea
                placeholder="Enter bio..."
                value={bio}
                maxLength={maxWords}
                onChange={handleBioChange}
                id={post.postText}
                autosize={{ minRows: 3, maxRows: 6 }} />
            <Progress showInfo={false} percent={100 * (bio.length / maxWords)} />
        </div>,
        <div style={styles}>
            <ProfileImage className={post.profile} src={image} />
            <div style={{ display: 'flex', flexDirection: "row" }}>
                <Upload showUploadList={false} accept="image/*" beforeUpload={handleImage}>
                    <Button icon='picture' shape='round' type='primary'>Upload</Button>
                </Upload>
                <Button onClick={() => setImage('')} icon="delete" type="danger" shape="circle" style={{ marginLeft: 10 }} />
            </div>
        </div>
    ]

    const setupClose = () => {
        props.setupClose()
    }

    const footer = <div>
        <Button shape="round" disabled={stage === 0} type="default" onClick={() => setStage(stage - 1)}>Back</Button>
        {stage === 0 ?
            <Button shape="round" type="primary" onClick={() => setStage(stage + 1)}>Next</Button>
            :
            <Button shape="round" type="primary" onClick={() => handleSubmit()}>Submit</Button>
        }
    </div>

    return (
        <Modal maskClosable={false} visible={props.app.setupModal} onCancel={() => setupClose()} footer={footer}>
            <div style={{ padding: '20px 20px 0px 20px' }}>
                <Steps current={stage}>
                    <Step title="Bio" />
                    <Step title="Profile image" />
                </Steps>
            </div>
            {stages[stage]}
        </Modal>
    );
}


const mapStateToProps = state => ({
    app: state.app,
    user: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setupClose: setupClose,
    updateUser: updateUser
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post)