import React from 'react'
import { Input, Tooltip } from 'antd'
import search from '../../../style/RightPanel.module.css'

const Search = (props) => {


    return (
            <Tooltip placement="bottom" title='Use "@" for users, and "#" for topics'>
                <Input.Search
                    placeholder="Search"
                    onSearch={value => console.log(value)}
                    className={search.search}
                />
            </Tooltip>
    )
}

export default Search