const express = require("express")
const http = require("http")
const socketIo = require("socket.io")
const index = require("./routes/index")
const bodyParser = require('body-parser')
const socketioJwt = require('socketio-jwt')
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose')
const ObjectID = require('mongodb').ObjectID

mongoose.connect(process.env.DB || 'mongodb://localhost:27017/dev', { useFindAndModify: false, useUnifiedTopology: true, useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('connected to database'))

const port = process.env.PORT || 3001
const secret = process.env.SECRET || 'VERYSECRET'

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(index)

const server = http.createServer(app)
const io = socketIo(server)

let users = {}

let interval = 1

const conversationSchema = new mongoose.Schema({
    timestamp: 'string',
    users: ['string'],
    read: ['string'],
    messages: ['object'],
})

const Conversation = mongoose.model('Conversation', conversationSchema);

const emitContent = (username, payload) => {
    /**
     * todo
     * find socket id with username
     * emit payload to recipients
     */
}

app.post('/messaging/send', (req, res) => {
    try {
        const decoded = jwt.verify(req.headers.authorization.split(' ')[1], secret);
        const sender = decoded.identity.username
        const message = {
            _id: new ObjectID(),
            sender: sender,
            image: req.body.image,
            text: req.body.text,
            timestamp: new Date().toUTCString()
        }
        Conversation.findOneAndUpdate({ _id: ObjectID(req.query.id) }, {
            '$push': {
                'messages': message
            }, '$set': { 'timestamp': new Date().toUTCString(), read: [sender] }
        }).exec((err, model) => {
            if (err) {
                res.status(400).json({ ok: false, message: err.message })
            } else {
                model.users.forEach(user => {
                    if (user !== sender) {
                        io.emit(users[user], {new: false, ...message, chatId: req.query.id})
                    }
                })
                res.status(200).json({ ok: true, data: message })
            }
        })
    } catch (err) {
        res.status(401).json({ ok: false, message: err.message })
    }
})

app.get('/messaging/inbox', (req, res) => {
    try {
        const decoded = jwt.verify(req.headers.authorization.split(' ')[1], secret);
        const sender = decoded.identity.username
        Conversation.find({ 'users': { '$elemMatch': { '$eq': sender } } })
            .sort([['timestamp', -1]]).exec((err, model) => {
                if (err) {
                    res.status(400).json({ ok: false, message: err.message })
                } else {
                    res.status(200).json({ ok: true, data: model })
                }
            })
    } catch (err) {
        res.status(401).json({ ok: false, message: err.message })
    }
})

app.post('/messaging/create', (req, res) => {
    try {
        const decoded = jwt.verify(req.headers.authorization.split(' ')[1], secret);
        const sender = decoded.identity.username
        const msgObject = {
            _id: new ObjectID(),
            timestamp: new Date().toUTCString(),
            users: [sender, req.body.recipient],
            read: [sender],
            messages: [{
                _id: new ObjectID(),
                timestamp: new Date().toUTCString(),
                sender: sender,
                text: req.body.text,
                image: req.body.image
            }]
        }
        const message = new Conversation(msgObject)
        message.save((err, model) => {
            if (err) {
                res.status(400).json({ ok: false, message: err.message })
            } else {
                if (err) {
                    res.status(400).json({ ok: false, message: err.message })
                } else {
                    model.users.forEach(user => {
                        if (user !== sender) {
                            io.emit(users[user], {new: true, ...msgObject})
                        }
                    })
                    res.status(200).json({ ok: true, data: message })
                }
            }
        })
    } catch (err) {
        res.status(401).json({ ok: false, message: err.message })
    }
})

io.sockets
    .on('connection', socketioJwt.authorize({
        secret: secret,
        timeout: 15000 // 15 seconds to send the authentication message
    })).on('authenticated', function (socket) {
        //this socket is authenticated, we are good to handle more events from it.
        // ToDo: get socket id to send to individual users
        // ToDo: add socket id to user map
        const sessionID = socket.id
        const username = socket.decoded_token.identity.username
        console.log(`${username} connected with session id: ${sessionID}`)
        users[username] = sessionID

        // todo emit userlist ever 10s
    
        if (!!interval) {
            clearInterval(interval)
        }
        //interval = setInterval(socket.emit("onlineUsers", Object.keys(users)), 1000)
        socket.on("disconnect", () => {
            delete users[username]
            console.log(`${username} disconnected`)
        })
    });

server.listen(port, () => console.log(`Listening on port ${port}`))
