import os
import copy
from bson import ObjectId
from flask import request, jsonify
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies
)
from datetime import datetime
from app import app, mongo, flask_bcrypt, jwt
from app.schemas.post import (validate_post, validate_comment)
import logger

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))

page = 10


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@app.route('/api/v1/post', methods=['POST', 'GET', 'DELETE'])
@jwt_required
def post():
    current_user = get_jwt_identity()
    if request.method == 'POST':
        data = validate_post(request.get_json(force=True))
        if data['ok']:
            data = data['data']
            data['timestamp'] = datetime.utcnow()
            data['username'] = current_user['username']
            data['saves'] = []
            _id = mongo.db.posts.insert_one(data).inserted_id
            data['_id'] = _id
            return jsonify({'ok': True, 'data': data}), 200
        else:
            LOG.info('Post was not created, bad parameters: {}'.format(
                data['message']))
            return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400
    if request.method == 'GET':
        query = request.args
        query_filter = {}
        for key in query:  # filters out the 'page' key
            if key != 'page':
                query_filter[key] = query[key]
        data = mongo.db.posts.find(query_filter).sort([('timestamp', -1)]).skip(page * int(request.args.get('page'))
                                                                                ).limit(page)
        posts = []
        for doc in data:
            doc['_id'] = str(doc['_id'])
            posts.append(doc)
        if len(posts) == 0:
            return jsonify({'ok': True, 'data': []}), 204
        return jsonify({'ok': True, 'data': posts}), 200
    if request.method == 'DELETE':
        mongo.db.posts.delete_one(
            {'username': current_user['username'], '_id': ObjectId(request.args.get('id'))})
        return jsonify({'ok': True}), 200


@app.route('/api/v1/heart', methods=['POST'])
@jwt_required
def heart():
    current_user = get_jwt_identity()
    mongo.db.posts.update_one({'_id': ObjectId(request.args.get('id'))}, {
        '$addToSet': {'hearts': current_user['username']}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/unheart', methods=['POST'])
@jwt_required
def unheart():
    current_user = get_jwt_identity()
    mongo.db.posts.update_one({'_id': ObjectId(request.args.get('id'))}, {
        '$pull': {'hearts': current_user['username']}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/save', methods=['POST'])
@jwt_required
def save():
    current_user = get_jwt_identity()
    mongo.db.posts.update_one({'_id': ObjectId(request.args.get('id'))}, {
        '$addToSet': {'saves': current_user['username']}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/unsave', methods=['POST'])
@jwt_required
def unsave():
    current_user = get_jwt_identity()
    mongo.db.posts.update_one({'_id': ObjectId(request.args.get('id'))}, {
        '$pull': {'saves': current_user['username']}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/saved', methods=['GET'])
@jwt_required
def saved():
    current_user = get_jwt_identity()
    query = request.args
    query_filter = {}
    for key in query:  # filters out the 'page' key
        if key != 'page':
            query_filter[key] = query[key]
    data = mongo.db.posts.find({'saves': current_user['username']}).sort([('timestamp', -1)]).skip(page * int(request.args.get('page'))
                                                                                                   ).limit(page)
    posts = []
    for doc in data:
        doc['_id'] = str(doc['_id'])
        posts.append(doc)
    if len(posts) == 0:
        return jsonify({'ok': True, 'data': []}), 204
    return jsonify({'ok': True, 'data': posts}), 200


@app.route('/api/v1/comment', methods=['POST', 'DELETE'])
@jwt_required
def comment():
    current_user = get_jwt_identity()
    reqId = request.args.get('id')
    commentId = request.args.get('commentId')
    LOG.info(reqId)
    LOG.info(commentId)
    LOG.info(request.method)
    if request.method == 'POST':
        data = validate_comment(request.get_json(force=True))
        if data['ok']:
            data = data['data']
            data['_id'] = ObjectId()
            data['timestamp'] = datetime.utcnow()
            mongo.db.posts.update_one({'_id': ObjectId(reqId)}, {
                '$addToSet': {'comments': data}})
            return jsonify({'ok': True, 'data': data}), 200
        else:
            LOG.info('Post was not created, bad parameters: {}'.format(
                data['message']))
            return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400
    if request.method == 'DELETE':
        mongo.db.posts.update_one(
            {'_id': ObjectId(reqId)},
            {'$pull': {'comments': {'_id': ObjectId(commentId), 'username': current_user['username']}}})
        return jsonify({'ok': True}), 200
